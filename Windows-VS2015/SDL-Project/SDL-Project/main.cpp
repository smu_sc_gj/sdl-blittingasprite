/*
  Based on http://lazyfoo.net/SDL_tutorials/
*/

//Include SDL library
//This is now cross platform. 
#if defined(WIN32)
    #include "SDL.h"
#else
    #include "SDL/SDL.h"
#endif

const int SCREEN_WIDTH=640; //Screen width
const int SCREEN_HEIGHT=480; //Screen height
const int SCREEN_BPP=32;    //Screen bits per pixel

int main( int argc, char* args[] )
{
    //Declare pointers to surface structures
    SDL_Surface* rawImage = NULL;
    SDL_Surface* background = NULL;
    SDL_Surface* screen = NULL;

    //Initialise SDL
    //NOTE: SDL_INIT_EVERYTHING is a constant defined by SDL
    SDL_Init( SDL_INIT_EVERYTHING );

    //Surface structure we'll use for the window - screen
    //This is essentially the back buffer. 
    screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );

    //Load our bitmap image
    rawImage = SDL_LoadBMP( "beefy-miracle.bmp" );

    if(rawImage != NULL) //if the image is not null
    {
        //Optimise
        background = SDL_DisplayFormat(rawImage);

        //Dispose of rawImage - we have our optimised copy
        SDL_FreeSurface(rawImage);
    }
    else
    {
        printf("Failed to load image");
        exit(1);
    }

    //Blit the image to the screen
    //Draw to backbuffer
    SDL_BlitSurface( background, NULL, screen, NULL );

    //Refresh the screen (replace with backbuffer). 
    SDL_Flip( screen );

    //Pause to allow the image to be seen
    SDL_Delay( 10000 );

    //Free the loaded bitmap
    SDL_FreeSurface( background );

    //Shutdown SDL - clear up resorces etc. 
    SDL_Quit();

    return 0;
}
